// TODO
// 1. Die
// 2. Pause / resume
// 3. Points

var mWidth = 640;
var mHeight = 480;
var nx = 40;
var ny = 30;
var frame_rate = 10;

function display_block(p) {
	var i = p[0];
	var j = p[1];

	rect(i*mWidth/nx, j*mHeight/ny, mWidth/nx, mHeight/ny);
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getRandomPosition() {
	return [getRandomInt(0, nx-1), getRandomInt(0,ny-1)];
}

// var c = color(255,0,0);

var snake = {
	body: [[1,1], [2,1], [3,1], [4,1], [5,1], [6,1], [6,2], [6,3]],
	velocity: "s",
	body_color: "#ff0000",
	food_color: "#00ff00",
	food : getRandomPosition(),
	show: function() {
		clear();
		// console.log(color(this.body_color));
		fill(color(this.food_color));
		display_block(this.food);

		fill(color(this.body_color));
		for (var i = 0; i<this.body.length; i++) {
			display_block(this.body[i]);
		}
	},
	update: function() {
		//move snake ahead
		var curTail = this.body[0];
		for(var i=0; i<this.body.length-1; i++) {
			this.body[i] = this.body[i+1];						
		}

		var ohx = this.body[this.body.length-1][0];
		var ohy = this.body[this.body.length-1][1];
		var headx, heady;
		var velocity = this.velocity;
		if (this.food[0] == ohx && this.food[1] == ohy) {
			console.log("Length before : " + this.body.length);

			console.log("eatten");
			this.body = [curTail].concat(this.body);
			this.food = getRandomPosition();

			console.log("Length After : " + this.body.length);

		}


		if (velocity == "w") {
			headx = ohx;
			heady = ohy - 1;
		} else if (velocity == "a") {
			headx = ohx - 1;
			heady = ohy;
		} else if (velocity == "s") {
			headx = ohx;
			heady = ohy + 1;
		} else if (velocity == "d") {
			headx = ohx + 1;
			heady = ohy;
		}
		headx = ((headx % nx) + nx) % nx ;
		heady = ((heady % ny) + ny) % ny ;
		this.body[this.body.length-1] = [headx, heady];
		// console.log(headx, heady);
	},
	updateVelocity: function(key) {
		// console.log("updating velocity : " + key);
		if (this.velocity == 'w' || this.velocity == "s") {
			if (key=="a" || key=="d") {
				this.velocity = key;
			}
		}
		if (this.velocity == 'a' || this.velocity == "d") {
			if (key=="s" || key=="w") {
				this.velocity = key;
			}
		}
		// console.log(this.velocity);
	}
}


function setup() {
	console.log(frame_rate);
	createCanvas(mWidth, mHeight);
	// createCanvas(640, 480);
	frameRate(frame_rate);
}

function draw() {	
	snake.show();
	snake.update();
}

function keyPressed() {
  if (keyCode == UP_ARROW) {
    // fillVal = 255;
    // console.log("up!");
    snake.updateVelocity("w");
  } else if (keyCode == DOWN_ARROW) {
    // fillVal = 0;
    // console.log("down");
    snake.updateVelocity("s");
  }else if (keyCode == LEFT_ARROW) {
    // fillVal = 0;
    // console.log("left");
    snake.updateVelocity("a");
  }else if (keyCode == RIGHT_ARROW) {
    // fillVal = 0;
    // console.log("right");
    snake.updateVelocity("d");
  }
  return false; // prevent default
}
